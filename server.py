import requests
import requests_cache
import connexion
from swagger_ui_bundle import swagger_ui_3_path
from flask import render_template

requests_cache.install_cache('xml_resource_cache', backend='sqlite', expire_after=24*60*60)

# Create the application instance
app = connexion.App(__name__, specification_dir='./',options={'swagger_path': swagger_ui_3_path})

flask_app = app.app

# Configurations
flask_app.config.from_object('config.development')
flask_app.config.from_pyfile('instance/config.py')

# Read the swagger.yml file to configure the endpoints
app.add_api('swagger.yml',strict_validation = True)

# Create a URL route in our application for "/"
@app.route('/')
def home():
    """
    This function just responds to the browser URL localhost:5000/
    :return:        the rendered template 'home.html'
    """
    return render_template('home.html')

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

import requests
from lxml import etree
from hashlib import sha256
class UNData:
    _instance = None
    xml_url = "https://scsanctions.un.org/resources/xml/en/consolidated.xml"

    def __new__(clss):
        if clss._instance is None:
            clss._instance = super(UNData,clss).__new__(clss)
            clss._instance.xml_url = clss.xml_url
            clss._instance.update_data()
        return clss._instance

    def update_data(self):
        self.xml_root, self.sha_256 = self.parse_xml(self.xml_url)

    def parse_xml(self,xml_url):
        try:
            response = requests.get(xml_url)
            sha_256 = sha256(response.content).hexdigest()
            root = etree.ElementTree(etree.fromstring(response.content)).getroot()
        except Exception as e:
            raise e
        return root, sha_256

    def get_root(self):
        return self.xml_root
    
    def get_url(self):
        return self.xml_url
    
    def get_sha256(self):
        return self.sha_256
from flask import abort
from itertools import islice
import xmltodict
from lxml import etree
from hashlib import sha256

from .un_data import UNData

def search(search_string="",page_number=1,page_size=20):
    try:
        un_data = UNData()
        root = un_data.get_root()
        offset = (page_number - 1)*page_size
        if search_string == "" or search_string is None:
            results = ( element for element in root.iter("INDIVIDUAL"))
        else: 
            results = ( element for element in root.iter("INDIVIDUAL") if any( (element.findtext(field) is not None and search_string.upper() in element.findtext(field) for field in ["FIRST_NAME","SECOND_NAME","THIRD_NAME","FOURTH_NAME"])))
        return {'resource url' : un_data.get_url(),
                'sha256' :un_data.get_sha256(),
                'results': [ xmltodict.parse(etree.tostring(element, encoding='unicode')) for element in list(islice(results,offset,offset+page_size+1)) ],
                'page number' :  page_number,
                'page size' : page_size,
                }
    except Exception as e:
        abort(404)